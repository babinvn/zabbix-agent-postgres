FROM zabbix/zabbix-agent:alpine-latest

USER root

# Create credentials file .pgpass from environment variable on startup
COPY docker-entrypoint.sh.patch /tmp/
# Fix ZBX-17009 bug
COPY pgsql.dbstat.sql.patch /tmp/

RUN \
  apk add --no-cache --clean-protected git postgresql-client && \
  git clone --depth 1 https://github.com/zabbix/zabbix.git ~/zabbix && \
  cp ~/zabbix/templates/db/postgresql/template_db_postgresql.conf /var/lib/zabbix/ && \
  cp -r ~/zabbix/templates/db/postgresql/postgresql /var/lib/zabbix/ && \
  chown -R zabbix:root /var/lib/zabbix/postgresql && \
  rm -rf ~/zabbix && \
  patch /usr/bin/docker-entrypoint.sh < /tmp/docker-entrypoint.sh.patch && \
  patch /var/lib/zabbix/postgresql/pgsql.dbstat.sql < /tmp/pgsql.dbstat.sql.patch && \
  rm -f /tmp/docker-entrypoint.sh.patch

# Example, actual password should be different
ENV PG_CONN="postgres:5432:postgres:zbx_monitor:password"
