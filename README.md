# Zabbix Agent Postgres

For details about using Zabbix to monitor PostgreSQL database please refer to
https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/postgresql

This projects adds PostgreSQL database client to official Zabbix Agent image:
https://hub.docker.com/r/zabbix/zabbix-agent/

To specify database connection credentials use PG_CONN environment variable:

```
docker run --name some-zabbix-agent -d \
-e ZBX_HOSTNAME="some-hostname" \
-e ZBX_SERVER_HOST="some-zabbix-server" \
-e PG_CONN="postgres:5432:postgres:zbx_monitor:password" \
zabbix-agent-postgres
```
